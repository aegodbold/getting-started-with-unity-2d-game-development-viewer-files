This repository contains the files for viewers of the video course [Getting Started with Unity 2D Game Development](https://www.packtpub.com/game-development/getting-started-unity-2d-game-development). 
I created this repository so that I could easily update the files based on viewer feedback.
